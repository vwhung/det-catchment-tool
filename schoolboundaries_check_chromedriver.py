from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import time
import requests


def check_website_status(url):
    request = requests.get(url)
    if request.status_code == 404:
        print('404 error - server cannot find request')
    elif request.status_code == 500:
        print('500 error')
    elif request.status_code == 401:
        print('401 error - unauthorised')

def if_searchAdd_box_exists(driver):
    try:
        driver.find_element_by_xpath("""//*[@id="geocoder"]/div/input""")
    except NoSuchElementException:
        return False
    return True
        
def if_searchSchl_box_exists(driver):
    try:
        driver.find_element_by_xpath("""//*[@id="school-search"]/form/div/div/input""")
    except NoSuchElementException:
        return False
    return True
        
def search_by_address(input_address):
    address = driver.find_element_by_xpath("""//*[@id="geocoder"]/div/input""")
    address.send_keys(input_address)
    time.sleep(2)
    address.send_keys(Keys.ENTER)
    time.sleep(2)

def search_for_school(input_school):
    school = driver.find_elements_by_xpath("""//*[@id="school-search"]/form/div/div/input""")[-1]
    school.send_keys(input_school)
    time.sleep(2)
    school.send_keys(Keys.ENTER)
    time.sleep(2)
    
def catchment_primary_button():
    try:
        driver.find_element_by_xpath("""//*[@id="BoundarySelector"]/label""").click()
        time.sleep(2)
    except WebDriverException:
        print('Primary school button is not clickable')

def catchment_secondary():
    # check secondary school button
    try:
        driver.find_element_by_xpath("""//*[@id="sec"]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Secondary school button is not clickable')
    
    # check individual years
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[1]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Yr7 school button is not clickable')
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[2]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Yr8 school button is not clickable')
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[3]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Yr9 school button is not clickable')
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[4]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Yr10 school button is not clickable')
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[5]/label""").click()
        time.sleep(1)
    except WebDriverException:
        print('Yr11 school button is not clickable')
    try:
        driver.find_element_by_xpath("""//*[@id="secondary-years"]/span[6]/label""").click()
        time.sleep(2)
    except WebDriverException:
        print('Yr12 school button is not clickable')


def getInformation(bsArt):
    artRow = list()    
    name = bsArt.find('h2').string
    artRow.append(name)

    return artRow

def check_info_box(driver):
    pageSource = driver.page_source
    bsObj = BeautifulSoup(pageSource,"html.parser")
    info_box = bsObj.findAll('div',{'id':'school-info'})
    for i in info_box:
        x = getInformation(i)
        if len(x) ==0:
            print('No school content returned')


options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')

chromedriver = r'C:\Users\vhung\Downloads\chromedriver_win32\chromedriver'
driver = webdriver.Chrome(executable_path=chromedriver, chrome_options=options)
driver.get("http://qa:DTpUDAiq7dpXx66s@schoolboundaries.crowdspot.com.au")#visit site
time.sleep(2)

check_website_status('http://qa:DTpUDAiq7dpXx66s@schoolboundaries.crowdspot.com.au')

if if_searchAdd_box_exists(driver) == True:
    #check the search-by-address box
    search_by_address('7 Riverside Quay, Southbank')

    #check if School type buttons exist and are clickable
    catchment_primary_button()
    catchment_secondary()

    check_info_box(driver)
else:
    print('Cannot find search by address box')
    
if if_searchSchl_box_exists(driver) == True:
    #check the search for school box
    search_for_school("Sandringham")
    check_info_box(driver)
else:
    print('Cannot find search by school box')

driver.close()
print('Checking completed!')